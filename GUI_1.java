import javax.swing.JOptionPane;

public class GUI_1
{
	public static void main(String args[])
	{
		String num_1 = JOptionPane.showInputDialog("Enter First Integer");
		String num_2 = JOptionPane.showInputDialog("Enter Second Integer");

		int n_1 = Integer.parseInt(num_1);
		int n_2 = Integer.parseInt(num_2);

		int sum = n_1 + n_2;

		JOptionPane.showMessageDialog(null,"The sum is "+sum, "Sum of two Integers", JOptionPane.PLAIN_MESSAGE);
	}
}